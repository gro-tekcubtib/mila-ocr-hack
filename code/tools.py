'''
Author: Alex-Antoine Fortin
Date: Friday, June 8th 2019
Description
Useful functions for image processing
https://github.com/mzucker/noteshrink/blob/master/noteshrink.py
'''
from PIL import Image
import numpy as np

def change_contrast(img, level):
    factor = (259 * (level + 255)) / (255 * (259 - level))
    def contrast(c):
        value = 128 + factor * (c - 128)
        return max(0, min(255, value))
    return img.point(contrast)

#for levels in range(1, -300, -5):
#    img = change_contrast(Image.open('../inputs/training_set/30.jpg'), level=levels)
#    img.save(f'../inputs/contrasting/30-{levels}.jpg')


#===============================================================================

def quantize(image, bits_per_channel=None):
    '''Reduces the number of bits per channel in the given image.'''
    if bits_per_channel is None:
        bits_per_channel = 6
    assert image.dtype == np.uint8
    shift = 8-bits_per_channel
    halfbin = (1 << shift) >> 1
    return ((image.astype(int) >> shift) << shift) + halfbin

#===============================================================================

def pack_rgb(rgb):
    '''Packs a 24-bit RGB triples into a single integer,
works on both arrays and tuples.'''
    orig_shape = None
    if isinstance(rgb, np.ndarray):
        assert rgb.shape[-1] == 3
        orig_shape = rgb.shape[:-1]
    else:
        assert len(rgb) == 3
        rgb = np.array(rgb)
    rgb = rgb.astype(int).reshape((-1, 3))
    packed = (rgb[:, 0] << 16 |
              rgb[:, 1] << 8 |
              rgb[:, 2])
    if orig_shape is None:
        return packed
    else:
        return packed.reshape(orig_shape)

#===============================================================================

def unpack_rgb(packed):
    '''Unpacks a single integer or array of integers into one or more
24-bit RGB values.
    '''
    orig_shape = None
    if isinstance(packed, np.ndarray):
        assert packed.dtype == int
        orig_shape = packed.shape
        packed = packed.reshape((-1, 1))
    rgb = ((packed >> 16) & 0xff,
           (packed >> 8) & 0xff,
           (packed) & 0xff)
    if orig_shape is None:
        return rgb
    else:
        return np.hstack(rgb).reshape(orig_shape + (3,))

#===============================================================================
def get_bg_color(image, bits_per_channel=None):
    '''Obtains the background color from an image or array of RGB colors
by grouping similar colors into bins and finding the most frequent
one.
    '''
    assert image.shape[-1] == 3
    quantized = quantize(image, bits_per_channel).astype(int)
    packed = pack_rgb(quantized)
    unique, counts = np.unique(packed, return_counts=True)
    packed_mode = unique[counts.argmax()]
    return unpack_rgb(packed_mode)

#===============================================================================
def rgb_to_sv(rgb):
    '''Convert an RGB image or array of RGB colors to saturation and
value, returning each one as a separate 32-bit floating point array or
value.
    '''
    if not isinstance(rgb, np.ndarray):
        rgb = np.array(rgb)
    axis = len(rgb.shape)-1
    cmax = rgb.max(axis=axis).astype(np.float32)
    cmin = rgb.min(axis=axis).astype(np.float32)
    delta = cmax - cmin
    saturation = delta.astype(np.float32) / cmax.astype(np.float32)
    saturation = np.where(cmax == 0, 0, saturation)
    value = cmax/255.0
    return saturation, value

#===============================================================================
def get_fg_mask(bg_color, samples, value_threshold, sat_threshold):
    '''Determine whether each pixel in a set of samples is foreground by
comparing it to the background color. A pixel is classified as a
foreground pixel if either its value or saturation differs from the
background by a threshold.'''
    s_bg, v_bg = rgb_to_sv(bg_color)
    s_samples, v_samples = rgb_to_sv(samples)
    v_diff = np.abs(v_bg - v_samples)
    s_diff = np.abs(s_bg - s_samples)
    #print(np.percentile(v_diff, range(75,100,1)))
    return ((v_diff >= value_threshold) |
            (s_diff >= sat_threshold))

#===============================================================================


if __name__ == '__main__':
    import os
    import cv2
    inputPath = '../inputs/training_set/'
    outputPath = '../inputs/contrasting_train/'
    value_threshold = 0.05
    fileList = [f for f in os.listdir(inputPath) if (os.path.isfile(os.path.join(inputPath, f)) and '._' not in f)]
    for idx, filename in enumerate(fileList):
        if idx%1000==0:
            print(f'processing {idx}th image')
        img = np.array(Image.open(os.path.join(inputPath, f'{filename}')))
        img = cv2.fastNlMeansDenoisingColored(img,None,10,10,7,21) #TODO: not tested; might want to comment this out.
        imgCopy = img.copy()
        bg = np.array(get_bg_color(img))
        mask = get_fg_mask(bg_color=bg, samples=img, value_threshold=value_threshold, sat_threshold=1000)
        #print('percentage-of-higher-than-{}-threshold: {}'.format(value_threshold, sum(sum(mask))/np.prod(mask.shape)))
        np.copyto(dst=img, src=np.zeros(shape=img.shape), where=np.repeat(mask[:, :, None], 3, axis=2), casting='unsafe')
        np.copyto(dst=img, src=np.ones(shape=img.shape)*255, where=np.repeat(~mask[:, :, None], 3, axis=2), casting='unsafe')
        Image.fromarray(np.hstack([img,imgCopy])).save(os.path.join(outputPath, f'{filename}'))
